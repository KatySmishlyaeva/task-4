package com.example.task4

import android.Manifest

const val MY_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION
const val LOCATION_REQUEST_CODE = 100