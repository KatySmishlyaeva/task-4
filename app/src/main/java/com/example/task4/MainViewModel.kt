package com.example.task4

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.task4.api.provideApi
import com.example.task4.model.ATMItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel : ViewModel() {
    val listATM = MutableLiveData<List<ATMItem>>()

    init {
        getListATMs()
    }

    private fun getListATMs() {
        provideApi().getATM().enqueue(object : Callback<List<ATMItem>> {
            override fun onResponse(call: Call<List<ATMItem>>, response: Response<List<ATMItem>>) {
                if (response.isSuccessful) {
                    listATM.value = response.body()
                }
            }

            override fun onFailure(call: Call<List<ATMItem>>, t: Throwable) {
                Log.d("MainViewModel", t.message.toString())
            }
        })
    }
}
