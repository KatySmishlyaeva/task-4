package com.example.task4

import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.example.task4.databinding.ActivityMapsBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterManager

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var viewModel: MainViewModel
    private lateinit var map: GoogleMap
    private lateinit var binding: ActivityMapsBinding
    private lateinit var clusterManager: ClusterManager<MyItem>
    private val gomel = LatLng(52.4251, 30.9927)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(gomel, 11f))

        map.uiSettings.apply {
            isZoomControlsEnabled = true
            isMyLocationButtonEnabled = true
        }

        checkLocationPermission()
        setUpClustererOfATM()
    }

    private fun setUpClustererOfATM() {
        clusterManager = ClusterManager(this, map)
        map.setOnCameraIdleListener(clusterManager)

        viewModel.listATM.observe(this, {
            for (atmItem in it) {
                val lat = atmItem.gps_x.toDouble()
                val lng = atmItem.gps_y.toDouble()
                val snippet =
                    "${atmItem.address_type} ${atmItem.address} ${atmItem.house}"
                clusterManager.addItem(
                    MyItem(
                        lat,
                        lng,
                        getString(R.string.atm_belarusbank),
                        snippet
                    )
                )
            }
        })
    }

    private fun checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                MY_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        )
            map.isMyLocationEnabled = true
        else {
            requestPermission()
        }
    }

    private fun requestPermission() {
        val permission = arrayOf(MY_LOCATION)
        ActivityCompat.requestPermissions(this, permission, LOCATION_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LOCATION_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                map.isMyLocationEnabled = true
            } else {
                Toast.makeText(this, getString(R.string.permission_denied), Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }
}
