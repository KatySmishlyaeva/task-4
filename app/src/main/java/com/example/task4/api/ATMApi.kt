package com.example.task4.api

import com.example.task4.model.ATMItem
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ATMApi {
    @GET("atm?city=Гомель")
    fun getATM(): Call<List<ATMItem>>
}

internal fun provideApi(): ATMApi {
    val baseURL = "https://belarusbank.by/api/"
    val loginInterceptor = HttpLoggingInterceptor()
        .setLevel(HttpLoggingInterceptor.Level.BODY)
    val okHttpClient = OkHttpClient.Builder()
        .addInterceptor(loginInterceptor)
        .build()

    val retrofit = Retrofit.Builder()
        .baseUrl(baseURL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(okHttpClient)
        .build()

    return retrofit.create(ATMApi::class.java)
}